<!DOCTYPE html>
<html lang="pt-br">
<head>
<!-- Hotjar Tracking Code for https://grupoecher.com.br/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1939469,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- THEME CSS -->
<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">  
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Fonts Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">	
    
    <!--Google Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;500;700&display=swap" rel="stylesheet">
    
    <title>
        <?php if(is_front_page() || is_home()){
            $sep =" &raquo ";
            echo get_bloginfo('name'), $sep; bloginfo('description');
            }else {
                    echo wp_title('');
                    }    
                        ?>  
    </title>
    <?php wp_head(); ?> 
	    <link href="https://evaecherbot.azurewebsites.net/css/botchat.css" rel="stylesheet" />

    <div id="bot"></div>    
	
</head>
    <body>
<header>
	<nav class="nav-admin">
        <div class="container">
            <div class="row justify-content-between">
            <div class="col-md-3 ligue-busca">
                <div class="row">
                    <div class="col-md-12 pr-0 ligue text-center">
                        <span class="gray">Ligue agora</span><br/>
					<?php $telefone = preg_replace('/[^\w]/', '', get_field('telefone', 'option'));?>
              <a href="tel:+55<?php echo $telefone;?>" title="Telefone: <?php the_field('telefone','option');?>"><small class="gray"><i class="fas fa-phone fa-flip-horizontal"></i> </small> <span class="blue bold"><?php the_field('telefone','option');?></span></a>
                    </div>  
           <!--   <div class="col-md-8">
                         <form role="search" method="get" id="search-form"  action="<?php echo esc_url( home_url( '/' ) ); ?>">
							  <input type="hidden" name="search" value="advanced">
    <div class="search-wrap">
    	 <input type="search" placeholder="Busque seu imóvel" class="form-control" name="cidade" id="search-input" required value="<?php echo esc_attr( get_search_query() ); ?>" />
       
    </div>
</form>
                    </div> -->
                </div>
            </div>   
			<div class="col-md-8 ">
				
				<div class="row h-100">
            <div class="col-md-3 pt-3 d-none d-md-block trabalhe text-center">
                <a class="gray" href="<?php echo site_url('/trabalhe-conosco/');?>">Trabalhe Conosco</a>
            </div>   
            <div class="col-md-2 pt-3 d-none d-md-block corretor text-center">
               <a  class="gray" href="https://ofertas.grupoecher.com.br/lp-corretor-vender-echer" target="_blank">Corretor</a>
            </div>  
				         <div class="col-auto pt-3 d-none d-md-block compramos-terreno text-center">
               <a  class="white" href="https://ofertas.grupoecher.com.br/compramos-o-seu-terreno" target="_blank">Compramos seu terreno</a>
            </div>  
            <div class="col-md-3 pt-3 d-none d-md-block cliente text-center">
                <a class="white" href="<?php echo site_url('/portal-do-cliente/');?>"><i class="fas fa-lock"></i> Login do cliente</a>
            </div>   
				</div>
				</div>
        </div>
    </div>
</nav>
<nav class="navbar container navbar-expand-lg navbar-light" id="main-nav">
<?php the_custom_logo();
      if (!has_custom_logo()):?>
    <h1><?php bloginfo('name'); ?></h1>
<?php endif;?>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
  </button>

  <?php
  wp_nav_menu( array(
      'theme_location'  => 'primary',
      'depth'           => 2,
      'container'       => 'div',
      'container_id'    => 'navbarNavDropdown',
      'container_class' => 'collapse  navbar-collapse',
      'menu_class'      => 'ml-auto text-center nav navbar-nav',
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'walker'          => new WP_Bootstrap_Navwalker()
  ) );
  ?>
</nav>       
</header>