<?php get_header();
    if (have_posts()) :
        while (have_posts()) : the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-full' ); ?>
<div class="banner-single-empreendimento" style="background-image:url('<?php echo $image[0]; ?>)">
    <div class="container h-100">
			<div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-8">
                    <div class="titulos text-center">
                        <h1 class="white bold"><?php the_title(); ?></h1>
                            <h2 class="white">Crescimento contínuo e valorização da equipe</h2>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class="bg-gray">
<article class="container page-trabalhe py-5">
    <div class="row">
        <div class="col-md-8">
        <?php the_content();?>
        </div>
        <div class="col-md-4">
                <?php if( have_rows('tags_trabalhe_conosco') ):
                    while ( have_rows('tags_trabalhe_conosco') ) : the_row();?>
                   <div class="box-check">
                <p><i class="fas fa-check"></i> <?php the_sub_field('tags');?></p>
                         </div>
                <?php endwhile; endif;?>          
        </div>
    </div>
</article>
</div>
<section class="container mt-5">
    <div class="row">
        <div class="col-12 default-page ">
            <h2 class="text-center gray mb-5">Informações sobre a Vaga</h2>
<div id="accordion">
                    <?php if( have_rows('vagas') ):
                        $accordion = 0;
                    while ( have_rows('vagas') ) : the_row();?>
      
   
    
<div class="heading" id="heading_<?php echo $accordion; ?>">
      <h5 class="mb-0">
        <button class="btn btn-link <?php if (  $accordion != 0): echo 'collapsed'; endif; ?>" data-toggle="collapse" data-target="#collapse_<?php echo $accordion; ?>" aria-expanded="<?php if (  $accordion == 0): echo 'true'; else: echo 'false'; endif; ?>" aria-controls="collapse_<?php echo $accordion; ?>">
         <?php the_sub_field('nome_vaga');?>
       
        </button>
      </h5>
    </div>
    <div id="collapse_<?php echo $accordion; ?>" class="collapse <?php if (  $accordion == 0): echo 'show"';endif ?>" aria-labelledby="heading_<?php echo $accordion; ?>" data-parent="#accordion">
      <div class="card-body">
          <h6 class="bold gray">Sobre a Vaga</h6>
            <?php the_sub_field('sobre_a_vaga');?>
      </div>
    </div>
              <?php $accordion++; endwhile; endif;?>      
</div>
        </div>
    
    </div>
</section>
<section class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-10 default-page ">
            <h2 class="text-center gray mb-5">Cadastre-se para uma Vaga</h2>
            <?php echo do_shortcode('[contact-form-7 id="630" title="Trabalhe conosco"]');?>
        </div>
    </div>
</section>
<?php endwhile; else: ?>
<p><?php _e('Desculpe, não há posts a exibir.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>