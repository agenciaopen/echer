<?php get_header();
    if (have_posts()) :
        while (have_posts()) : the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-full' ); ?>
<div class="banner-single-empreendimento" style="background-image:url('<?php echo $image[0]; ?>)">
    <div class="container h-100">
			<div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-8">
                    <div class="titulos text-center">
                        <h1 class="white bold"><?php the_title(); ?></h1>
                            <h2 class="white"><?php the_field('subtitle');?></h2>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class="bg-gray-linear-sobre">
    <article class="container page-trabalhe">
        <div class="row py-6">
            <div class="col-md-6 ">
                <?php the_content();?>
            </div>
            <div class="col-md-6">
                <?php $image= get_field('imagem_lateral'); 
                    if (!empty($image)):?>
                <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" class="w-100 b-radius-18" />
            <?php endif; ?>
            </div>
        </div>
    </article>
        <div class="container">
            <div class="box-counts">
				            <div class="row " id="progress">
                <div class="col-md-3 separators">
                    <h3 class="Count"><?php the_field('anos_de_historia');?></h3>
                    <h5>Anos de história</h5>
                </div>
                <div class="col-md-3 separators">
                    <h3 class="Count"><?php the_field('unidades_entregues');?></h3>
                    <h5>Unidades entregues</h5>
                </div>
                <div class="col-md-3 separators">
                <h3 class="Count"><?php the_field('em_construcao');?></h3>
                    <h5>Em Construção</h5>
                </div>        
                <div class="col-md-3 separators last">
                <h3 class="Count"><?php the_field('sonhos_realizados');?></h3>
                    <h5>Sonhos realizados</h5>
                </div>
                </div>
            </div>
        </div>
    </div>

<section class="container mt-5" >
    <div class="row">
        <div class="col-12 default-page ">
            <h2 class="text-center gray my-5">Missão, Visão e Valores</h2>
            <div id="accordion">
    <div class="heading" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
  Missão
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
			<?php the_field('missao');?>
      </div>
    </div>
    <div class="heading" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         Visão
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
			<?php the_field('visao');?>
      </div>
    </div>

    <div class="heading" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
Valores
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
			<?php the_field('valores');?>
      </div>
    </div>
    <div class="heading" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
Política de qualidade
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
		<?php the_field('politica_de_qualidade');?>
      </div>
    </div>				
</div>
        </div>
    </div>
</section>
<section class="container mt-5">
    <div class="row">
        <div class="col-12 default-page ">
            <h2 class="text-center gray mt-5">Nossa abrangência</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-9 map-echer">
        <img src="<?php the_field('imagem_mapa');?>" class="img-fluid"/>
        <div class="box-legenda">
            <h3>CIDADES DA ABRANGÊNCIA</h3> 
        <ul>
			<?php
				if( have_rows('legenda') ):
			$i=0;
    while( have_rows('legenda') ) : the_row(); $i++;?>
<li><a href="<?php the_sub_field('link_legenda');?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/pins/pin-<?php echo $i;?>.png"/> <?php the_sub_field('nome_legenda');?></a></li>   
    <?php endwhile; endif;?>
        </ul>
        </div>
        </div>
    </div>
</section>
<?php endwhile; else: ?>
<p><?php _e('Desculpe, não há posts a exibir.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
<script>
    jQuery(function($) {
    var $section = $('#progress');
    var $queue = $({});
    
    function loadDaBars() {
$('.Count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1800,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
    }
    
    $(document).bind('scroll', function(ev) {
        var scrollOffset = $(document).scrollTop();
        var containerOffset = $section.offset().top - window.innerHeight;
        if (scrollOffset > containerOffset) {
            loadDaBars();
            // unbind event not to load scrolsl again
            $(document).unbind('scroll');
        }
    });
    
});
    </script>




