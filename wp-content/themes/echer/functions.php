<?php
// Add theme support for Featured Images
add_theme_support('post-thumbnails', array(
'post',
'page',
'empreendimentos',
));


function scripts_do_template() {
    wp_register_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'));
    wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'));
    wp_enqueue_script('popper');
    wp_enqueue_script('bootstrap');
}
add_action('wp_footer', 'scripts_do_template');


function theme_widgets_init() {
	register_sidebar( array(
		'name'          => 'Right Sidebar',
		'id'            => 'sidebar-primary',
		'before_widget' => '<div class="widget-right">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'theme_widgets_init' );

function pagination_bar() {
    global $wp_query;
    $total_pages = $wp_query->max_num_pages;
     if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
         echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}

function get_excerpt(){
$excerpt = get_the_content();
$excerpt = strip_shortcodes($excerpt);
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, 120);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
$excerpt = $excerpt.'...';
return $excerpt;
}


/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

register_nav_menus( array(
    'primary' => __( 'Menu Primário', 'Main Menu' ),
) );

/* OPTIONS PAGE */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Informações do site',
		'menu_title'	=> 'Informações do site',
	));
	
}

function mytheme_setup() {
    add_theme_support('custom-logo');
}

add_action('after_setup_theme', 'mytheme_setup');


/**
 * Mudar classe do logo
 */
function custom_logo_output( $html ) {
	$html = str_replace('custom-logo-link', 'navbar-brand', $html );
	return $html;
}
add_filter('get_custom_logo', 'custom_logo_output', 10);


function add_custom_filter_wp_title( $title ) {
    if ( '' == $title ) {
        return get_bloginfo( 'name' );
    }
    return $title;
}
add_filter( 'wp_title', 'add_custom_filter_wp_title' );

add_theme_support( 'html5', array( 'search-form' ) );

//CUSTOM JS E CSS
function ekkoJs() {
if ( is_singular( 'empreendimentos' ) ) {
    wp_register_script('ekkojs', get_template_directory_uri() .'/inc/ekko-lightbox.min.js');
	wp_enqueue_script('ekkojs');
}
}
 add_action("wp_footer", "ekkoJs");

function register_style_lightbox() {
if ( is_singular( 'empreendimentos' ) ) {
    wp_enqueue_style( 'lightbox', get_template_directory_uri() . '/inc/ekko-lightbox.css' );
}
}
add_action( 'wp_enqueue_scripts', 'register_style_lightbox' );

// CUSTOM SEARCH EMPREENDIMENTO
function wpse_load_custom_search_template(){
    if( isset($_REQUEST['search']) == 'advanced' ) {
        require('advanced-search-result.php');
        die();
    }
}
add_action('init','wpse_load_custom_search_template');


				
function load_scripts(){
if ( is_singular( 'empreendimentos' ) ) {    
              wp_enqueue_style( 'slick', get_template_directory_uri() . '/dist/slick.css' );
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/dist/slick-theme.css' );
		//OWL Carousel
		wp_enqueue_script('slick-js', get_template_directory_uri() . '/dist/js/slick.js', array('jquery'), null, true);

		//Custom JS
		wp_enqueue_script('custom-js', get_template_directory_uri() . '/dist/js/app.js', array('jquery', 'slick-js'), null, true);
	}
}
	add_action('wp_enqueue_scripts','load_scripts');

?>