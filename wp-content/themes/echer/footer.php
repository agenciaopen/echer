    <div class="bg-gray-linear mt-10">
<section class="container">
   <?php if(!is_home() && !is_singular('post') && !is_archive() && !is_category() ): ?>
<div class="newsletter">
    <div class="row py-5 justify-content-center">
        <div class="col-md-5 px-5 form-news">
            <?php echo do_shortcode('[contact-form-7 id="106" title="Newsletter Promocional"]');?>
            <small><i class="fas fa-lock"></i> Seus dados estão protegidos conosco</small>
        </div>
        <div class="col-md-5 px-5 form-news">
            <h2>Cadastre-se em nossa Newsletter</h2>
            <p>Receba nossas novidades e promoções diretamente no seu e-mail</p>
        </div>
    </div> 
</div>
<? else : ?>
  <div class="newsletter">
    <div class="row py-5 justify-content-center">
        <div class="col-md-5 px-5 form-news">
            <?php echo do_shortcode('[contact-form-7 id="1075" title="Newsletter Blog"]');?>
            <small><i class="fas fa-lock"></i> Seus dados estão protegidos conosco</small>
        </div>
        <div class="col-md-5 px-5 form-news">
            <h2>Cadastre-se em nossa Newsletter</h2>
            <p>Receba nossos conteúdos inéditos e totalmente gratuitos no seu e-mail</p>
        </div>
    </div> 
</div>  
<?php endif;?>
</section>
    <?php if(!is_home() && !is_singular('post') && !is_archive() && !is_category() ): ?>
<footer class="container mt-4">
    <?php else :?>
    <footer class="container mb-0 mt-4">
    <?php endif;?>
    <div class="row">
        <div class="col-md-3">
         <?php the_custom_logo();
      if (!has_custom_logo()):?>
    <h1><?php bloginfo('name'); ?></h1>
<?php endif;?>
        </div>
        <div class="col-md-4 copy">
            <p>Todos os direitos reservados ao Grupo Echer - <?php echo date('Y');?><br/>
            <a class="gray" href="https://grupoecher.com.br/politica-de-privacidade/" target="_blank"><small>Política de Privacidade</small></a>

        </p>
        </div>
        <div class="col-md-5 mt-4">
            <div class="row">
                <div class="col-md-5">
                <p>Siga o Grupo Echer nas Redes Sociais</p>
                </div>
                <div class="col-md-7 redes-footer text-center">
<a class="linkSocial" href="<?php the_field('facebook','option');?>" rel="noopener" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
 <a class="linkSocial" href="<?php the_field('instagram','option');?>" rel="noopener" title="Instagram" target="_blank">
	 <i class="fab fa-instagram" aria-hidden="true"></i></a>
 <a class="linkSocial" href="<?php the_field('linkedin','option');?>" rel="noopener" title="Linkedin" target="_blank">
	<i class="fab fa-linkedin-in"></i></a>
 <a class="linkSocial" href="<?php the_field('youtube','opgtion');?>" rel="noopener" title="Youtube" target="_blank">
	<i class="fab fa-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row unidades mt-4 pb-4">
		<?php if( have_rows('adicionar_endereco', 'option') ):
   			 while( have_rows('adicionar_endereco', 'option') ) : the_row(); ?>
   	<div class="col-sm">
            <h5 class="gray"><?php the_sub_field('titulo', 'option');?></h5>
            <p><?php the_sub_field('endereco', 'option');?><br/>
		<?php  $telefone = preg_replace('/[^\w]/', '', get_sub_field('telefone', 'option'));?>
				<a href="tel:+55<?php echo $telefone;?>" class="tel-unidade"><?php the_sub_field('telefone', 'option');?></a></p>
        </div>    
			<?php endwhile; endif; ?>

    </div>

</footer>
			<div id="chat-eva">
        <img id="mychat" src="<?php echo get_template_directory_uri();?>/dist/images/chat-corretor-online-on.png"/>		
</div>
    <?php if(!is_home() && !is_singular('post') && !is_archive() && !is_category() ): ?>
<div class="bg-blue fixed-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-3 px-0 text-center">
            <div class="bg-yellow">
              <a class="simu-footer" href="#." data-toggle="modal" data-target="#simularHome">
                  <img src="<?php echo get_template_directory_uri();?>/dist/images/money.svg"/>
                <p class="bold">Faça uma simulação</p>
                </a>
            </div>
            </div>
            <a href="mailto:<?php the_field('e-mail','option');?>" class="col-md-4 text-center links-f">
                <h6>Enviar um e-mail</h6>
                <span><i class="far fa-envelope"></i><?php the_field('e-mail','option');?></span>
            </a>
			   	<?php $whatsapp = preg_replace('/[^\w]/', '', get_field('whatsapp', 'option'));?>
                  <a href="#." data-toggle="modal" data-target="#WppF" class="col-sm text-center links-f">
                <h6>Falar por Whatsapp</h6>
              	<span><i class="fab fa-whatsapp"></i><?php the_field('whatsapp','option');?></span> 
            </a>   
				<?php  $telefone = preg_replace('/[^\w]/', '', get_field('telefone', 'option'));?>
            <a href="tel:+55<?php echo $telefone;?>" class="col-sm text-center links-f">
		
                <h6>Quero comprar</h6>
               <span><i class="fas fa-phone fa-flip-horizontal"></i> <?php the_field('telefone','option');?></span>
			</a>
        </div>
    </div>
</div>
    <!-- Modal -->
<div class="modal fade" id="simularHome" tabindex="-1" role="dialog" aria-labelledby="Simular" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
<div role="main" id="faca-sua-simulacao-066148e9d0b6672612f6"></div>
<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
<script type="text/javascript">
  new RDStationForms('faca-sua-simulacao-066148e9d0b6672612f6-html', 'UA-123742090-1').createForm();
</script>
    </div>
  </div>
	</div>
</div>
        
<div class="modal fade" id="WppF" tabindex="-1" role="dialog" aria-labelledby="Simular" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body wppFb">
		          <a href="#." class="closeW" data-dismiss="modal" aria-label="Close">
                      X
        </a>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="bg-b">
                    <div class="wpp-c text-center">
                        <span><i class="fab fa-whatsapp"></i> ATENDIMENTO EXCLUSIVO POR WHATSAPP</span>
                    </div>
                    <h3 class="wpp-t">
                    Informe seu WhatsApp para falar com nossos especialistas.
                    </h3>
                    <p class="sub-wt">
                        Tire suas dúvidas sobre o empreendimento, faça uma simulação e conheça nossas ofertas especiais.
                    </p>
                        <?php echo do_shortcode('[contact-form-7 id="1627" title="Form Wpp"]');?>
                </div>
            </div>
            </div>  
        </div>
         <div class="exp">
             <div class="container h-100">
                <div class="row h-100 justify-content-center">
                    <div class="col-sm-7 my-auto text-center">
                                <div class="wpp-c text-center">
                        <span><i class="fab fa-whatsapp"></i> ATENDIMENTO EXCLUSIVO POR WHATSAPP</span>
                    </div>
            <h3 class="wpp-t mt-4">
   
                   Olá <span id="varName"></span> um de nossos especialistas já está pronto para falar com você!
                    </h3>
                              <p class="sub-wt">
                   Clique no botão abaixo para iniciar a conversa
                    </p>
            <?php $whatsapp = preg_replace('/[^\w]/', '', get_field('whatsapp', 'option'));?>
                  <a id="click-wpp" href="https://api.whatsapp.com/send?phone=5565<?php echo $whatsapp;?>" target="_blank">
                    	<span><i class="fab fa-whatsapp"></i> Iniciar conversa agora!</span> 
            </a>  
                    </div>
                 </div>
             </div>
  
        </div>

    </div>

  </div>
	</div>
</div>
    <?php endif ?>
<?php wp_footer();?> 
    <script src="https://cdn.botframework.com/botframework-webchat/latest/CognitiveServices.js"></script>
    <script src="https://cdn.botframework.com/botframework-webchat/master/botchat.js"></script>
<script>
    (function () {
        var div = document.createElement("div");
		function guid() {
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        var user = {
            id: guid().toUpperCase(),
            name: 'User-' + Math.floor((1 + Math.random()) * 10000)
        };
		var botConnection = new BotChat.DirectLine({
            token: 'T64u45YxNxo.N9kVyqPdScua0TMxsRfatNhSNd8xBkurJRotPsv39H4',
            user: user
        });
        
        document.getElementsByTagName('body')[0].appendChild(div);
        div.outerHTML = "<div id='botDiv' style='width: 600px; height: 0px; margin:10px; position: fixed; bottom: 0; right:0; z-index: 1000;><div  id='botTitleBar' style='height: 40px; width: 600px; position:fixed; '></div></div>";
        BotChat.App({
            botConnection: botConnection,
            user: user,	    	
            bot: { id: 'botid', name: 'bot name' },
	    resize: 'detect'
        }, 
		document.getElementById("botDiv"));		
        document.getElementsByClassName("wc-header")[0].setAttribute("id", "chatbotheader");
		document.getElementsByClassName("wc-header")[0].innerHTML = "<span>Bate papo!</span>";
        document.getElementsByClassName("wc-header")[0].innerHTML += "<button type='button' onclick='doClose()' style='top: 20%; right:2%; position:absolute; color: #FFFFFF; cursor: pointer;border: none'>X</button>";        
        document.getElementById("mychat").addEventListener("click", function (e) 
        {
           document.getElementById("botDiv").style.height = '80%';           
		   document.getElementById("botDiv").style.width = '30%';
           document.getElementById("botDiv").style.minWidth = "400px";
           e.target.style.display = "none";
		   botConnection
            .postActivity({
                from: user,
                name: 'setUserIdEvent',
                type: 'event',
                value: 'setUserIdEvent'
            })
            .subscribe(function (id) {
                console.log('"trigger setUserIdEvent" sent');
            });        
        })
    }());
</script>
<script>
function doClose() {
var botDiv = document.querySelector('#botDiv');
                botDiv.style.height = "0px";
                document.getElementById("mychat").style.display = "inline";
}
</script>
<script type="text/javascript">
        var cor = "";
        var styleElem = document.head.appendChild(document.createElement("style"));
        styleElem.innerHTML = ".wc-message.wc-message-from-bot:before {content: url('https://iterupstorage.blob.core.windows.net/avatar/13.png')}";

        function dataCallback(data) {
            cor = data.Cor;
            jQuery('.wc-header').css("background-color", cor);
        }
        jQuery(document).on('DOMNodeInserted', function (e) {
            jQuery('.wc-card button').css("background-color", cor);
            jQuery('.wc-app button').css("background-color", cor);
            jQuery('.wc-message-from-me .wc-message-content').css("background-color", cor);
            jQuery('.wc-console .wc-mic, .wc-console .wc-send').css("background-color", "#ffffff");
            jQuery('.wc-card button').mouseover(function () {
                jQuery(this).css("background-color", "#ffffff");
                jQuery(this).css("border-color", cor);
                jQuery(this).css("color", cor);
            });
            jQuery('.wc-card button').mouseout(function () {
                jQuery(this).css("background-color", cor);
                jQuery(this).css("border-color", "#ffffff");
                jQuery(this).css("color", "#ffffff");
            });
        });
    </script>
        
    <!-- Faz download dacor do txt do azure blob - primeiro passo-->
    <script type="text/javascript" src="https://iterupstorage.blob.core.windows.net/avatar/13.txt"></script>
<script type="text/javascript">
  document.addEventListener( 'wpcf7mailsent', function( event ) {
      if ( '1627' == event.detail.contactFormId ) { // if you want to identify the form
          var hid = document.getElementsByClassName("exp");
    // Emulates jQuery $(element).is(':hidden');
    if(hid[0].offsetWidth > 0 && hid[0].offsetHeight > 0) {
        hid[0].style.visibility = "visible";
    }
      }
  }, true );
</script>
        
            <script>
document.addEventListener( 'wpcf7submit', function( event ) {
    var inputs = event.detail.inputs;

    for ( var i = 0; i < inputs.length; i++ ) {
        if ( 'nome' == inputs[i].name ) {
            document.getElementById("varName").innerHTML = ( inputs[i].value );
            /*
            or in console:
            console.log( inputs[i].value );
            */
            break;
        }
    }
}, false );
</script>         
        
  </body>
</html>