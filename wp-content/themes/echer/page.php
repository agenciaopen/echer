<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-full' ); ?>
<?php if (!empty($image)):?>
<div class="banner-single-empreendimento" style="background-image:url('<?php echo $image[0]; ?>)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
                <h1 class="white bold"><?php the_title();?></h1>
            </div>
        </div>
    </div>
</div>
<?php else :?>
    <div class="banner-no-image">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
                <h1 class="bold"><?php the_title();?></h1>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<article class="container default-page my-5">
    <div class="row justify-content-center">
        <div class="col-md-10">
        <?php the_content();?>
        </div>
    </div>
</article>
<?php endwhile; else: ?>
<p><?php _e('Desculpe, não há posts a exibir.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>