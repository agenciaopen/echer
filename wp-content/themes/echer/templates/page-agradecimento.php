<?php /* Template Name: Thank You Page */?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-full' ); ?>
<article class="container thks-page my-5">
    <div class="row justify-content-center">
        <div class="col-md-10">
    <h1><?php the_title();?></h1>
        </div>
    </div>
        <div class="row mt-5 justify-content-center">
            <div class="col-md-8 content-tks text-center gray">
                <?php the_content()?>
            </div>
        </div>
            <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box-email text-center">
                   <i class="fas fa-envelope"></i>
                        <h4>Você recebeu nosso e-mail?</h4>
                    <p>Caso não encontre nosso e-mail na caixa de entrada, confira a caixa de spam e a lixeira</p>
                </div>
            </div>
        </div>
</article>
<?php endwhile; else: ?>
<p><?php _e('Desculpe, não há posts a exibir.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>