<ul class="comment-list comments">
    <?php
	wp_list_comments( array(
	    'style'      => 'ul',
	    'short_ping' => true,
            'callback' => 'comments'
	) );
     ?>
</ul><!-- .comment-list -->