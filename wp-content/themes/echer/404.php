<?php get_header(); ?>
<div class="container not-found">
    <div class="row justify-content-center">
        <div class="col-md-10 text-center">
        <h1> 404 </h1>        
        <p>A página que você tentou acessar não existe, se você acredita que isso seja um erro, por favor contate-nos</p>
            <p><a href="javascript:history.go(-1)" title="Voltar a página anterior">Voltar</a></p>
        </div>
    </div>
</div>
<?php get_footer(); ?>