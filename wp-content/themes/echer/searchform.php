<?php
/**
 * default search form
 */
?>
<form role="search" method="get" class="p-3" id="search-form"  action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-wrap-blog">
    	 <input type="search" placeholder="Buscar no blog" class="form-control" name="s" id="search-input" required value="<?php echo esc_attr( get_search_query() ); ?>" />
       
    </div>
</form>