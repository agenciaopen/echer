<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-full' ); ?>
<div class="banner-single-empreendimento" style="background-image:url('<?php echo $image[0]; ?>)">
    <div class="container h-100">
			<div class="row h-100  justify-content-center align-items-center">
				<div class="col-md-8 text-center">
                        <h1 class="white bold"><?php the_title();?></h1>
                <?php $post_date = get_the_date('d-m-Y');?>
                <div class="icons-b white">
                    <span><i class="far fa-comment-dots"></i> <?php comments_number('0 Comentários', '1 Comentário', '% Comentários' );?></span>     
                    <span><i class="far fa-calendar"></i> <?php echo $post_date;?></span> 
                    <span><i class="fas fa-power-off"></i> <?php echo get_the_author();?></span>
                </div> 
									</div>
				</div>
    </div>
</div> 
    
    
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
			<div class=" single-blog">
			<div class="bread">
			<?php $categories = get_the_category();?>
				<a class="category" href="<?php echo site_url('/blog/');?>">Blog <i class="fa fa-chevron-right" aria-hidden="true"></i></a> <span class="category">Categoria <i class="fa fa-chevron-right" aria-hidden="true"></i> </span>
			<span class="category list"><a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) );?>"><?php  echo esc_html( $categories[0]->name ); ?></a></span>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-10 mt-5">
            <h2 class="w-75"><?php the_title();?></h2>
            <hr class="my-4">
            <?php the_content();?>
			   <div class="row">
            <div class="col-12">
                    <hr>
            <p class="share-links"><strong>COMPARTILHAR:</strong>
                <a href="http://www.facebook.com/sharer.php?u=<?php echo the_permalink();?>" target="_blank"><i class="fab fa-facebook-square"></i></a>
               
                <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" target="_blank" data-show-count="false"><i class="fab fa-twitter-square"></i></a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                 <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>" target="_blank"><i class="fab fa-linkedin"></i></a>
                </p>
            </div>
        </div> 
				</div></div>
        </div>
    </div>
	</div>
</div>
   <section>
        <div class="container my-3">
		<div class="row justify-content-center">
			<div class="col-12">
			    <h5 class="gray bold px-2">Artigos relacionados</h5>
					<div class="row">
					<?php
$args_category = array(
 'category__in'   => wp_get_post_categories( $post->ID ),
        'posts_per_page' => 5,
        'post__not_in'   => array( $post->ID )
);
$filters_query = new WP_Query( $args_category );
if ( $filters_query->have_posts() ) :
while ( $filters_query->have_posts() ) : $filters_query->the_post();
$post_date = get_the_date('d-m-Y');
?>      
                             <div class="col-md-4 my-2">
								 <div class="related-post px-2">
                					 <span class="category"><?php the_category();?></span>
                                     <img  src="<?php  echo get_the_post_thumbnail_url() ?>" class="py-2 w-100"/>
                                    <div class="icons-b gray">
                                <span><i class="far fa-comment-dots"></i> <?php comments_number('0 Comentários', '1 Comentário', '% Comentários' );?>   </span>   
                                <span><i class="far fa-calendar"></i><?php echo $post_date;?></span>
                                <span><i class="fas fa-power-off"></i><?php echo get_the_author();?></span>
                                    </div> 
                                    <h5 class="bold gray"><?php the_title();?></h5>
                                    <p><?php  echo get_excerpt();?></p>
                                    <p><a class="bold" href="<?php the_permalink();?>">Leia mais>></a></p>
                            </div>
			</div>
                        <?php endwhile; endif; wp_reset_query();?>
				</div></div></div>
            <div class="row">
                <div class="col-md-10">
                <hr class="my-5">
                 <h5 class="gray bold px-2">Comentários:</h5>
                 <?php comments_template(); ?> 
                </div>
            </div>
       </div>
</section>

<?php endwhile; else: ?>
<p><?php _e('Desculpe, não há posts a exibir.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>