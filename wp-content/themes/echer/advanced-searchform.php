<?php 
$query = array(
'posts_per_page' => -1,
'post_type'		=> 'empreendimentos',
'order'                  => 'ASC',
 'orderby'                => 'title',
	
);
$the_query = new WP_Query( $query );
 if( $the_query->have_posts() ): 
  while( $the_query->have_posts() ) : $the_query->the_post();
 $cidades[] = get_field('cidade');
 endwhile; endif;
$cidades = array_unique( $cidades );?>

<form method="get" id="advanced-searchform" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <label>Busque seu imóvel</label>
                    <input type="hidden" name="search" value="advanced">
                <div class="form-row">
                    <div class="col-sm mb-2">
                         <select class="form-control" name="cidade" id="cidade">
                                    <option value="" disabled selected>Cidade</option>
			<?php	foreach ( $cidades as $cidade ) { ?>
       <option value="<?php echo str_replace(" ","&nbsp;","$cidade"); ?>"><?php echo $cidade; ?></option>

            
<?php } ?>
							 
                        </select>
                    </div>
               <!--     <div class="col-sm mb-2">
                         <select class="form-control" name="caracteristica" id="caracteristica">
                                    <option value="" disabled selected>Características</option>
                             <option value="2-quartos"><?php _e( '2 Quartos', 'textdomain' ); ?></option>
                             <option value="2-suite"><?php _e( '2 e 3 quartos c/ 1 suíte', 'textdomain' ); ?></option>
                        </select>
                    </div> -->
                    <div class="col-sm mb-2">
                                      <select class="form-control" name="tipo" id="tipo">
                                    <option value="" disabled selected>Tipo</option>
                             <option value="casa"><?php _e( 'Casa', 'textdomain' ); ?></option>
                             <option value="lote"><?php _e( 'Lote', 'textdomain' ); ?></option>
                        </select>
                    </div>
                  <!--  <div class="col-sm mb-2">
                       <select class="form-control" name="status" id="status">
                                    <option value="" disabled selected>Status</option>
                             <option value="em-construcao"><?php _e( 'Em construção', 'textdomain' ); ?></option>
                             <option value="lancamento"><?php _e( 'Lançamento', 'textdomain' ); ?></option>
                             <option value="pronto-para-morar"><?php _e( 'Pronto para morar', 'textdomain' ); ?></option>
                             <option value="na-planta"><?php _e( 'Na planta', 'textdomain' ); ?></option>
                        </select>
                    </div> -->
                    <div class="col-sm mb-2">
                        <button type="submit" class="btn btn-buscar w-100">Buscar</button>
                    </div>
                </div>
                </form>
<script>
var hashParams = window.location.hash.substr(1).split('&'); // substr(1) to remove the `#`
for(var i = 0; i < hashParams.length; i++){
    var p = hashParams[i].split('=');
    document.getElementById(p[0]).value = decodeURIComponent(p[1]);;
}
</script>