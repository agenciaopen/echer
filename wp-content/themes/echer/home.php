<?php get_header(); ?>
<?php if (have_posts()) :?>
  <?php $blog_page_image = wp_get_attachment_url( get_post_thumbnail_id(get_option( 'page_for_posts' )) );?>
<div class="banner-empreendimento" style="background-image:url('<?php echo $blog_page_image ?>')">
    <div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12 text-center">
               <h1 class="white bold">Blog do Grupo Echer</h1>
        <h2 class="white">Conteúdos Gratuitos todas as semanas</h2>
									</div>
				</div>
        <div class="row justify-content-center">
            <div class="col-12 box-form-blog">
					<div class="row justify-content-center">
						<div class="col-md-9">
							                <h5>Cadastre em nossa newsletter para receber os conteúdos no e-mail</h5>
                <?php echo do_shortcode('[contact-form-7 id="392" title="Newsletter Blog"]');?>
						</div>
				</div>

            </div>
        </div>
</div>
</div>
    <div class="container main-blog mt-5">
        <div class="row blog-row justify-content-between">
             <div class="col-md-7 ml-5">
                 <div class="row">
          <?php  while (have_posts()) : the_post(); $post_date = get_the_date('d-m-Y');?>
                     <div class="row box-blog-page justify-content-end">
						 <div class="img-off-box">
							 		 <a href="<?php the_permalink();?>" class="overlay-img blog">
										          <div class="lupa">
                          <i class="fas fa-search-plus"></i>
						</div>
                                     <img  src="<?php  echo get_the_post_thumbnail_url(); ?>"/>
						 </a>
						 </div>
				
                                <div class="col-md-6 box-content-feed pt-4 mx-5">
                                    <div class="icons-b mb-2">
                                <span><i class="far fa-comment-dots"></i> <?php comments_number('0 Comentários', '1 Comentário', '% Comentários' );?></span>   
                                <span><i class="far fa-calendar"></i> <?php echo $post_date;?></span>
                                <span><i class="fas fa-power-off"></i> <?php echo get_the_author();?></span>
                                    </div> 
                                    <h4 class="bold gray"><?php the_title();?></h4>
                                    <p><?php  echo get_excerpt();?></p>
                                    <p class="text-right"><a class="bold" href="<?php the_permalink();?>">Leia mais>></a></p>
                                </div>
                            </div>   
            <?php endwhile;?>
        </div>
    </div>
       <div class="col-md-4 h-100">
           <div class=" off-sidebar">
    <?php if ( is_active_sidebar( 'sidebar-primary' ) ) : ?>
	<ul id="sidebar pt-3">
	<?php get_sidebar('sidebar'); ?>
	</ul>
<?php endif; ?>
           <div class="bg-yellow form-sidebar mx-3">
                <h2 class="light">Assine nossa News e receba no e-mail</h2>
               <?php echo do_shortcode('[contact-form-7 id="1074" title="Newsletter Blog Lateral"]');?>
           </div>
		<div class="my-5 text-center">
			<a class="linkSocial" href="<?php the_field('facebook','option');?>" rel="noopener" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
 <a class="linkSocial" href="<?php the_field('instagram','option');?>" rel="noopener" title="Instagram" target="_blank">
	 <i class="fab fa-instagram" aria-hidden="true"></i></a>
			   </div>
    </div>
        </div>
            </div>
    </div>
<?php else: ?>
<p><?php _e('Desculpe, não há posts a exibir.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>    