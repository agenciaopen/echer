<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'echer_db_22' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'use_echer' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '12345' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'i[(1kkWqiMs=@(2KH[D^-M:~NE}:JA)[4tnZ#<=v`V}GsoNWq+dIWva`WrVmuH3b' );
define( 'SECURE_AUTH_KEY',  'e@)p@>Pn@ldM4!+Uuu4{(gicxD(6Xc02;yHD`,AIlAW4e.gt+,ZzVa|2H!2!aWro' );
define( 'LOGGED_IN_KEY',    '$eL#-IS^yB47ulMaYpPz!msWp!9!oFkNG_/r.S9ZD<Km}+7-zdXJ<Do^+Hf<=rd+' );
define( 'NONCE_KEY',        'Z49csir9gG?v@HNH$VO*di(2NxomV5+H=@2-vtrf?g(y1;1LQ@ 3M40v^Jsf4GEW' );
define( 'AUTH_SALT',        'r%.^F`kRX2^I6dkYL7F9u?C1FS$qe|lE(*t`T-#yd];|fN!D549fPTa-!ugf2xo0' );
define( 'SECURE_AUTH_SALT', 'Nk5>1-2rzNz oNJB7)I^zxfx;}Y U!MEo/qWmq<T17A~,S92anWIj{pC=Me|,.zZ' );
define( 'LOGGED_IN_SALT',   ';.2R<;,VOv7EfD4Y< ^s3vvyr8<[zFnl$B.+S|ZC2S0|h+b$2Avz w8!rk=2KGVR' );
define( 'NONCE_SALT',       'U%gB2R jR:F<FhUCt$iZzfE$Pj91sNr^M0S:D<R2Xv<7Z&*S5r#&I/KP7]@E1XwA' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'ec_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Adicione valores personalizados entre esta linha até "Isto é tudo". */



/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
